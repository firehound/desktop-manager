import Vue from "vue";
import Vuex, {CommitOptions, DispatchOptions, Payload, Store} from "vuex";
import {blurAction, BlurActionPayload} from "./WindowStore/Actions/blurAction";
import {closeAction, CloseActionPayload} from "./WindowStore/Actions/closeAction";
import {focusAction, FocusActionPayload} from "./WindowStore/Actions/focusAction";
import {maximizeAction, MaximizeActionPayload} from "./WindowStore/Actions/maximizeAction";
import {moveToAction, MoveToActionPayload} from "./WindowStore/Actions/moveToAction";
import {openAction, OpenActionPayload} from "./WindowStore/Actions/openAction";
import {registerAction, RegisterActionPayload} from "./WindowStore/Actions/registerAction";
import {resizeToAction, ResizeToActionPayload} from "./WindowStore/Actions/resizeToAction";
import {restoreAction, RestoreActionPayload} from "./WindowStore/Actions/restoreAction";
import {
    getActiveWindowUUIDGetter,
    getActiveWindowUUIDGetterSignature,
} from "./WindowStore/Getters/getActiveWindowUUIDGetter";
import {getContentGetter, getContentGetterSignature} from "./WindowStore/Getters/getContentGetter";
import {getFrameGetter, getFrameGetterSignature} from "./WindowStore/Getters/getFrameGetter";
import {getIdsGetter, getIdsGetterSignature} from "./WindowStore/Getters/getIdsGetter";
import {
    getLastActiveWindowUUIDGetter,
    getLastActiveWindowUUIDGetterSignature,
} from "./WindowStore/Getters/getLastActiveWindowUUIDGetter";
import {getTitleGetter, getTitleGetterSignature} from "./WindowStore/Getters/getTitleGetter";
import {isActiveGetter, isActiveGetterSignature} from "./WindowStore/Getters/isActiveGetter";
import {isExistsGetter, isExistsGetterSignature} from "./WindowStore/Getters/isExistsGetter";
import {isMaximizedGetter, isMaximizedGetterSignature} from "./WindowStore/Getters/isMaximized";
import {blurMutation, BlurMutationPayload} from "./WindowStore/Mutations/blurMutation";
import {closeMutation, CloseMutationPayload} from "./WindowStore/Mutations/closeMutation";
import {focusMutation, FocusMutationPayload} from "./WindowStore/Mutations/focusMutation";
import {maximizeMutation, MaximizeMutationPayload} from "./WindowStore/Mutations/maximizeMutation";
import {moveToMutation, MoveToMutationPayload} from "./WindowStore/Mutations/moveToMutation";
import {openMutation, OpenMutationPayload} from "./WindowStore/Mutations/openMutation";
import {registerMutation, RegisterMutationPayload} from "./WindowStore/Mutations/registerMutation";
import {resizeToMutation, ResizeToMutationPayload} from "./WindowStore/Mutations/resizeToMutation";
import {restoreMutation, RestoreMutationPayload} from "./WindowStore/Mutations/restoreMutation";
import {initWindowStoreState, WindowStoreState} from "./WindowStore/WindowStoreState";

/*
 * ---------------------------------------------------------------------
 * | TYPE / INTERFACE DEFINITIONS
 * ---------------------------------------------------------------------
 */

export interface WindowStoreGetters {
    isExists: isExistsGetterSignature;
    isActive: isActiveGetterSignature;
    isMaximized: isMaximizedGetterSignature;
    getActiveWindowUUID: getActiveWindowUUIDGetterSignature;
    getLastActiveWindowUUID: getLastActiveWindowUUIDGetterSignature;
    getIds: getIdsGetterSignature;
    getTitle: getTitleGetterSignature;
    getFrame: getFrameGetterSignature;
    getContent: getContentGetterSignature;
}

interface WindowStoreConstraint extends Store<WindowStoreState> {
    readonly getters: WindowStoreGetters;

    commit(type: "register", payload: RegisterMutationPayload, options?: CommitOptions): void;

    commit(type: "open", payload: OpenMutationPayload, options?: CommitOptions): void;

    commit(type: "close", payload: CloseMutationPayload, options?: CommitOptions): void;

    commit(type: "focus", payload: FocusMutationPayload, options?: CommitOptions): void;

    commit(type: "blur", payload: BlurMutationPayload, options?: CommitOptions): void;

    commit(type: "moveTo", payload: MoveToMutationPayload, options?: CommitOptions): void;

    commit(type: "focusTo", payload: ResizeToMutationPayload, options?: CommitOptions): void;

    commit(type: "restore", payload: RestoreMutationPayload, options?: CommitOptions): void;

    commit(type: "maximize", payload: MaximizeMutationPayload, options?: CommitOptions): void;

    commit<P extends Payload>(payloadWithType: P, options?: CommitOptions): void;

    dispatch(type: "register", payload: RegisterActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "open", payload: OpenActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "close", payload: CloseActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "focus", payload: FocusActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "blur", payload: BlurActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "moveTo", payload: MoveToActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "resizeTo", payload: ResizeToActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "restore", payload: RestoreActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "maximize", payload: MaximizeActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch<P extends Payload>(payloadWithType: P, options?: DispatchOptions): Promise<any[]>;
}

/*
 * ---------------------------------------------------------------------
 * | STORE DEFINITION
 * ---------------------------------------------------------------------
 */

Vue.use(Vuex);

export const WindowStore: WindowStoreConstraint = new Store<WindowStoreState>({
    state: initWindowStoreState(),

    getters: {
        isExists: isExistsGetter,
        isActive: isActiveGetter,
        isMaximized: isMaximizedGetter,
        getActiveWindowUUID: getActiveWindowUUIDGetter,
        getLastActiveWindowUUID: getLastActiveWindowUUIDGetter,
        getIds: getIdsGetter,
        getTitle: getTitleGetter,
        getFrame: getFrameGetter,
        getContent: getContentGetter,
    },

    mutations: {
        register: registerMutation,
        open: openMutation,
        close: closeMutation,
        focus: focusMutation,
        blur: blurMutation,
        moveTo: moveToMutation,
        resizeTo: resizeToMutation,
        restore: restoreMutation,
        maximize: maximizeMutation,
    },

    actions: {
        register: registerAction,
        open: openAction,
        close: closeAction,
        focus: focusAction,
        blur: blurAction,
        moveTo: moveToAction,
        resizeTo: resizeToAction,
        restore: restoreAction,
        maximize: maximizeAction,
    },
});
