import Vue from "vue";
import {WindowStoreActionContext} from "../WindowStoreState";

export interface RegisterActionPayload {
    type: string;
    content: Vue;
}

export const registerAction = ({commit}: WindowStoreActionContext, payload: RegisterActionPayload): void => {
    commit("register", payload);
};
