import {WindowStoreActionContext} from "../WindowStoreState";

export interface OpenActionOptions {
    type: string;
    frame: {
        coordinate: {
            x: number
            y: number;
        }
        dimension: {
            width: number;
            height: number;
        };
    };
    maximized?: boolean;
    minDimension?: {
        width?: number;
        height?: number;
    };
    maxDimension?: {
        width?: number;
        height?: number;
    };
}

export interface OpenActionPayload {
    options: OpenActionOptions;
    callback?: (uuid: string) => void;
}

export const openAction = ({commit}: WindowStoreActionContext, payload: OpenActionPayload): void => {
    commit("open", payload);
};
