import {WindowStoreActionContext} from "../WindowStoreState";

/* tslint:disable:no-bitwise */
export enum ResizeDirection {
    TOP = 1 << 1,
    LEFT = 1 << 2,
    RIGHT = 1 << 3,
    BOTTOM = 1 << 4,
    TOP_LEFT = TOP | LEFT,
    TOP_RIGHT = TOP | RIGHT,
    BOTTOM_LEFT = BOTTOM | LEFT,
    BOTTOM_RIGHT = BOTTOM | RIGHT,
}

/* tslint:enable:no-bitwise */

export interface ResizeToActionPayload {
    uuid: string;
    direction: ResizeDirection;
    x?: number;
    y?: number;
}

export const resizeToAction = ({commit}: WindowStoreActionContext, payload: ResizeToActionPayload): void => {
    commit("resizeTo", payload);
};
