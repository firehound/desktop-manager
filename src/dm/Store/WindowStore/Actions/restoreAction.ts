import {WindowStoreActionContext} from "../WindowStoreState";

export interface RestoreActionPayload {
    uuid: string;
}

export const restoreAction = ({commit}: WindowStoreActionContext, payload: RestoreActionPayload): void => {
    commit("restore", payload);
};
