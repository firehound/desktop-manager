import {WindowStoreActionContext} from "../WindowStoreState";

export interface CloseActionPayload {
    uuid: string;
}

export const closeAction = ({commit}: WindowStoreActionContext, payload: CloseActionPayload): void => {
    commit("close", payload);
};
