import {WindowStoreActionContext} from "../WindowStoreState";

export interface MaximizeActionPayload {
    uuid: string;
}

export const maximizeAction = ({commit}: WindowStoreActionContext, payload: MaximizeActionPayload): void => {
    commit("maximize", payload);
};
