import {WindowStoreActionContext} from "../WindowStoreState";

export interface BlurActionPayload {
    uuid: string;
}

export const blurAction = ({commit}: WindowStoreActionContext, payload: BlurActionPayload): void => {
    commit("blur", payload);
};
