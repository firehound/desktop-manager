import {WindowStoreActionContext} from "../WindowStoreState";

export interface MoveToActionPayload {
    uuid: string;
    x: number;
    y: number;
}

export const moveToAction = ({commit}: WindowStoreActionContext, payload: MoveToActionPayload): void => {
    commit("moveTo", payload);
};
