import {WindowStoreActionContext} from "../WindowStoreState";

export interface FocusActionPayload {
    uuid: string;
}

export const focusAction = ({commit}: WindowStoreActionContext, payload: FocusActionPayload): void => {
    commit("focus", payload);
};
