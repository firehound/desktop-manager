import Vue from "vue";
import {ActionContext} from "vuex";

export interface Coordinate {
    x: number;
    y: number;
    z: number;
}

export interface Dimension {
    width: number;
    height: number;
}

export interface FrameState {
    coordinate: Coordinate;
    dimension: Dimension;
}

export interface WindowState {
    type: string;
    active: boolean;
    maximized: boolean;
    frame: FrameState;
    options: {
        minDimension: Dimension;
    };
}

export interface WindowStoreState {
    ids: string[];
    windowTypes: {
        [key: string]: {
            title: string;
            content: Vue;
        };
    };
    windowStates: {
        [key: string]: WindowState;
    };
}

export type WindowStoreActionContext = ActionContext<WindowStoreState, WindowStoreState>;

export const initWindowStoreState = (): WindowStoreState => {
    return {
        ids: [],
        windowTypes: {},
        windowStates: {},
    };
};
