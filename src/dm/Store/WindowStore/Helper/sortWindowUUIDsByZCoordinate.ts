import {WindowStoreState} from "../WindowStoreState";

export const sortWindowUUIDsByZCoordinate = (state: WindowStoreState): string[] => {
    return Object.keys(state.windowStates).sort((uuidA, uuidB) => {
        return state.windowStates[uuidA].frame.coordinate.z - state.windowStates[uuidB].frame.coordinate.z;
    });
};
