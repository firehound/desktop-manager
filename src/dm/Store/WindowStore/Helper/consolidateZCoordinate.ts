import {WindowStoreState} from "../WindowStoreState";
import {sortWindowUUIDsByZCoordinate} from "./sortWindowUUIDsByZCoordinate";

interface ConsolidateOptions {
    pushToEnd?: string;
}

export const consolidateZCoordinate = (state: WindowStoreState, options?: ConsolidateOptions): void => {
    options = options || {};

    const orderedWindowUUIDs = sortWindowUUIDsByZCoordinate(state);

    let nextZCoordinate = 1;

    orderedWindowUUIDs.forEach((uuid) => {
        if (!options.pushToEnd || options.pushToEnd !== uuid) {
            state.windowStates[uuid].frame.coordinate.z = nextZCoordinate++;
        }
    });

    if (options.pushToEnd) {
        state.windowStates[options.pushToEnd].frame.coordinate.z = nextZCoordinate++;
    }
};
