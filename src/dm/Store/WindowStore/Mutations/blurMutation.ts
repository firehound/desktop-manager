import {logger} from "@vi-kon/logger";
import {BlurActionPayload} from "../Actions/blurAction";
import {getLastActiveWindowUUIDGetter} from "../Getters/getLastActiveWindowUUIDGetter";
import {consolidateZCoordinate} from "../Helper/consolidateZCoordinate";
import {WindowStoreState} from "../WindowStoreState";

export type BlurMutationPayload = BlurActionPayload;

export const blurMutation = (state: WindowStoreState, {uuid}: BlurMutationPayload): void => {
    if (state.windowStates.hasOwnProperty(uuid)) {
        if (state.windowStates[uuid].active) {
            const previouslyActiveWindowUUID = getLastActiveWindowUUIDGetter(state)(1);
            if (previouslyActiveWindowUUID !== null) {
                consolidateZCoordinate(state, {pushToEnd: uuid});
                state.windowStates[uuid].active = true;
            }

            state.windowStates[uuid].active = false;

            logger.debug("Window blurred", {tags: ["window store"], uuid});
        }
    } else {
        logger.warn("Cannot blur on not existing window", {tags: ["window store"], uuid});
    }
};
