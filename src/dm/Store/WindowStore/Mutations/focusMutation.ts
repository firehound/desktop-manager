import {logger} from "@vi-kon/logger";
import {FocusActionPayload} from "../Actions/focusAction";
import {getActiveWindowUUIDGetter} from "../Getters/getActiveWindowUUIDGetter";
import {consolidateZCoordinate} from "../Helper/consolidateZCoordinate";
import {WindowStoreState} from "../WindowStoreState";

export type FocusMutationPayload = FocusActionPayload;

export const focusMutation = (state: WindowStoreState, {uuid}: FocusMutationPayload): void => {
    if (state.windowStates.hasOwnProperty(uuid)) {
        if (!state.windowStates[uuid].active) {
            const previouslyActiveWindowUUID = getActiveWindowUUIDGetter(state)();
            if (previouslyActiveWindowUUID) {
                state.windowStates[previouslyActiveWindowUUID].active = false;
            }

            consolidateZCoordinate(state, {pushToEnd: uuid});
            state.windowStates[uuid].active = true;

            logger.debug("Window focused", {tags: ["window store"], uuid});
        }
    } else {
        logger.warn("Cannot focus on not existing window", {tags: ["window store"], uuid});
    }
};
