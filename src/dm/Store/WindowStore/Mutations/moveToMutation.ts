import {logger} from "@vi-kon/logger";
import {MoveToActionPayload} from "../Actions/moveToAction";
import {WindowStoreState} from "../WindowStoreState";

export type MoveToMutationPayload = MoveToActionPayload;

export const moveToMutation = (state: WindowStoreState, {uuid, x, y}: MoveToMutationPayload): void => {
    if (state.windowStates.hasOwnProperty(uuid)) {
        state.windowStates[uuid].frame.coordinate.x = x;
        state.windowStates[uuid].frame.coordinate.y = y;

        logger.debug("Window moved to", {tags: ["window store"], uuid});
    } else {
        logger.warn("Cannot move not existing window to given position", {tags: ["window store"], uuid});
    }
};
