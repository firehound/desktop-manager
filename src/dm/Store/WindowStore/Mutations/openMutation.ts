import {logger} from "@vi-kon/logger";
import {v4 as uuidV4} from "uuid";
import Vue from "vue";
import {OpenActionPayload} from "../Actions/openAction";
import {getLastActiveWindowUUIDGetter} from "../Getters/getLastActiveWindowUUIDGetter";
import {consolidateZCoordinate} from "../Helper/consolidateZCoordinate";
import {Coordinate, Dimension, WindowState, WindowStoreState} from "../WindowStoreState";

export type OpenMutationPayload = OpenActionPayload;

export const openMutation = (state: WindowStoreState, {options, callback}: OpenMutationPayload): void => {
    if (state.windowTypes.hasOwnProperty(options.type)) {
        const uuid = uuidV4();

        const coordinate: Coordinate = {
            x: options.frame.coordinate.x,
            y: options.frame.coordinate.y,
            z: 0,
        };
        const dimension: Dimension = {
            width: options.frame.dimension.width,
            height: options.frame.dimension.height,
        };
        const minDimension: Dimension = {
            width: options.minDimension && options.minDimension.width ? options.minDimension.width : 0,
            height: options.minDimension && options.minDimension.height ? options.minDimension.height : 0,
        };
        const windowState: WindowState = {
            type: options.type,
            active: true,
            maximized: options.maximized || false,
            frame: {
                coordinate,
                dimension,
            },
            options: {
                minDimension,
            },
        };

        Vue.set(state.windowStates, uuid, windowState);

        state.ids.push(uuid);

        consolidateZCoordinate(state, {pushToEnd: uuid});

        const previouslyActiveWindowUUID = getLastActiveWindowUUIDGetter(state)(1);

        if (previouslyActiveWindowUUID !== null) {
            state.windowStates[previouslyActiveWindowUUID].active = false;
        }

        if (callback) {
            callback(uuid);
        }

        logger.debug("Window opened", {tags: ["window store"], uuid});
    } else {
        // TODO Need to indicated that type not found on the GUI

        logger.warn("Window with \"" + options.type + "\" type not found");
    }
};
