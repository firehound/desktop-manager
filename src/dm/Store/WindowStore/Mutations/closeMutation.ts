import {logger} from "@vi-kon/logger";
import {CloseActionPayload} from "../Actions/closeAction";
import {WindowStoreState} from "../WindowStoreState";

export type CloseMutationPayload = CloseActionPayload;

export const closeMutation = (state: WindowStoreState, {uuid}: CloseMutationPayload): void => {
    if (state.windowStates.hasOwnProperty(uuid)) {
        delete state.windowStates[uuid];

        state.ids.splice(state.ids.indexOf(uuid), 1);

        logger.debug("Window closed", {tags: ["window store"], uuid});
    } else {
        logger.warn("Cannot close not existing window", {tags: ["window store"], uuid});
    }
};
