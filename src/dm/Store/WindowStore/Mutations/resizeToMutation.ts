import {logger} from "@vi-kon/logger";
import {ResizeDirection, ResizeToActionPayload} from "../Actions/resizeToAction";
import {WindowStoreState} from "../WindowStoreState";

export type ResizeToMutationPayload = ResizeToActionPayload;

export const resizeToMutation = (state: WindowStoreState, {uuid, direction, x, y}: ResizeToMutationPayload): void => {
    if (state.windowStates.hasOwnProperty(uuid)) {
        const {frame, options} = state.windowStates[uuid];
        const {dimension, coordinate} = frame;
        const {minDimension} = options;

        /* tslint:disable-next-line:no-bitwise */
        if ((direction & ResizeDirection.TOP) !== 0) {
            const height = dimension.height + coordinate.y - y;

            // No need to reset y coordinate and height if min height is reached
            if (height < minDimension.height && dimension.height === minDimension.height) {
                return;
            }

            coordinate.y = height < minDimension.height
                ? y - (dimension.height - height)
                : y;

            dimension.height = height < minDimension.height
                ? minDimension.height
                : height;
        }

        /* tslint:disable-next-line:no-bitwise */
        if ((direction & ResizeDirection.RIGHT) !== 0) {
            const width = x - coordinate.x;

            // No need to reset height if min height is reached
            if (width < minDimension.width && dimension.width === minDimension.width) {
                return;
            }

            dimension.width = width < minDimension.width
                ? minDimension.width
                : width;

            dimension.width = width;
        }

        /* tslint:disable-next-line:no-bitwise */
        if ((direction & ResizeDirection.BOTTOM) !== 0) {
            const height = y - coordinate.y;

            // No need to reset height if min height is reached
            if (height < minDimension.height && dimension.height === minDimension.height) {
                return;
            }

            dimension.height = height < minDimension.height
                ? minDimension.height
                : height;
        }

        /* tslint:disable-next-line:no-bitwise */
        if ((direction & ResizeDirection.LEFT) !== 0) {
            const width = dimension.width + coordinate.x - x;

            // No need to reset x coordinate and width if min width is reached
            if (width < minDimension.width && dimension.width === minDimension.width) {
                return;
            }

            coordinate.x = width < minDimension.width
                ? x - (dimension.width - width)
                : x;

            dimension.width = width < minDimension.width
                ? minDimension.width
                : width;
        }

        logger.debug("Window resized", {tags: ["window store"], uuid});
    } else {
        logger.warn("Cannot resize not existing window", {tags: ["window store"], uuid});
    }
};
