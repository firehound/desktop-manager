import {MaximizeActionPayload} from "../Actions/maximizeAction";
import {WindowStoreState} from "../WindowStoreState";

export type MaximizeMutationPayload = MaximizeActionPayload;

export const maximizeMutation = (state: WindowStoreState, {uuid}: MaximizeMutationPayload): void => {
    state.windowStates[uuid].maximized = true;
};
