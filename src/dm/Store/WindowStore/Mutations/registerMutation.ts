import {RegisterActionPayload} from "../Actions/registerAction";
import {WindowStoreState} from "../WindowStoreState";

export type RegisterMutationPayload = RegisterActionPayload;

export const registerMutation = (state: WindowStoreState, {type, content}: RegisterMutationPayload): void => {
    state.windowTypes[type] = {
        title: "",
        content,
    };
};
