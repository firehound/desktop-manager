import {RestoreActionPayload} from "../Actions/restoreAction";
import {WindowStoreState} from "../WindowStoreState";

export type RestoreMutationPayload = RestoreActionPayload;

export const restoreMutation = (state: WindowStoreState, {uuid}: RestoreMutationPayload): void => {
    state.windowStates[uuid].maximized = false;
};
