import {WindowStoreState} from "../WindowStoreState";

export type isExistsGetterSignature = (uuid: string) => boolean;

export const isExistsGetter = (state: WindowStoreState): isExistsGetterSignature => {
    return (uuid: string): boolean => {
        return state.windowStates.hasOwnProperty(uuid);
    };
};
