import Vue from "vue";
import {WindowStoreGetters} from "../../WindowStore";
import {WindowStoreState} from "../WindowStoreState";

export type getContentGetterSignature = (uuid: string) => Vue | null;

export const getContentGetter = (state: WindowStoreState, getters: WindowStoreGetters): getContentGetterSignature => {
    return (uuid: string): Vue | null => {
        return getters.isExists(uuid) && state.windowTypes.hasOwnProperty(state.windowStates[uuid].type)
            ? state.windowTypes[state.windowStates[uuid].type].content
            : null;
    };
};
