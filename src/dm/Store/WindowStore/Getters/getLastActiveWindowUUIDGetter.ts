import {sortWindowUUIDsByZCoordinate} from "../Helper/sortWindowUUIDsByZCoordinate";
import {WindowStoreState} from "../WindowStoreState";

export type getLastActiveWindowUUIDGetterSignature = (index?: number) => string;

export const getLastActiveWindowUUIDGetter = (state: WindowStoreState): getLastActiveWindowUUIDGetterSignature => {
    return (index?: number): string => {
        index = index || 0;

        const orderedWindowUUIDs = sortWindowUUIDsByZCoordinate(state);

        if (index < orderedWindowUUIDs.length) {
            return orderedWindowUUIDs[index];
        }

        return null;
    };
};
