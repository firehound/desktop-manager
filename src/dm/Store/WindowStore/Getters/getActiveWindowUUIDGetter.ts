import {WindowStoreState} from "../WindowStoreState";

export type getActiveWindowUUIDGetterSignature = () => string | null;

export const getActiveWindowUUIDGetter = (state: WindowStoreState): getActiveWindowUUIDGetterSignature => {
    return (): string | null => {
        const foundUUID = Object.keys(state.windowStates).find((uuid) => {
            return state.windowStates[uuid].active;
        });

        return foundUUID || null;
    };
};
