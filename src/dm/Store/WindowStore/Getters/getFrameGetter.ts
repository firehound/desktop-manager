import {WindowStoreGetters} from "../../WindowStore";
import {FrameState, WindowStoreState} from "../WindowStoreState";

export type getFrameGetterSignature = (uuid: string) => FrameState | null;

export const getFrameGetter = (state: WindowStoreState, getters: WindowStoreGetters): getFrameGetterSignature => {
    return (uuid: string): FrameState | null => {
        return getters.isExists(uuid)
            ? state.windowStates[uuid].frame
            : null;
    };
};
