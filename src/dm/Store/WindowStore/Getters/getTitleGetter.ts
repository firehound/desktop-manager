import {WindowStoreGetters} from "../../WindowStore";
import {WindowStoreState} from "../WindowStoreState";

export type getTitleGetterSignature = (uuid: string) => string | null;

export const getTitleGetter = (state: WindowStoreState, getters: WindowStoreGetters): getTitleGetterSignature => {
    return (uuid: string): string | null => {
        return getters.isExists(uuid) && state.windowTypes.hasOwnProperty(state.windowStates[uuid].type)
            ? state.windowTypes[state.windowStates[uuid].type].title
            : null;
    };
};
