import {WindowStoreGetters} from "../../WindowStore";
import {WindowStoreState} from "../WindowStoreState";

export type isMaximizedGetterSignature = (uuid: string) => boolean;

export const isMaximizedGetter = (state: WindowStoreState, getters: WindowStoreGetters): isMaximizedGetterSignature => {
    return (uuid: string): boolean => {
        return getters.isExists(uuid) && state.windowStates[uuid].maximized;
    };
};
