import {WindowStoreState} from "../WindowStoreState";

export type getIdsGetterSignature = () => string[];

export const getIdsGetter = (state: WindowStoreState) => {
    return (): string[] => {
        return state.ids;
    };
};
