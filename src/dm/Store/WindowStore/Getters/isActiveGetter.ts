import {WindowStoreGetters} from "../../WindowStore";
import {WindowStoreState} from "../WindowStoreState";

export type isActiveGetterSignature = (uuid: string) => boolean;

export const isActiveGetter = (state: WindowStoreState, getters: WindowStoreGetters): isActiveGetterSignature => {
    return (uuid: string): boolean => {
        return getters.isExists(uuid) && state.windowStates[uuid].active;
    };
};
