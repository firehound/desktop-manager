import {ComponentOptions} from "vue";
import {DragEvent} from "../../../drag-and-drop/Components/Source";
import {WindowStore} from "../../Store/WindowStore";
import {ResizeDirection} from "../../Store/WindowStore/Actions/resizeToAction";

/* tslint:disable-next-line:no-var-requires */
const DraggableContent = require("../../../drag-and-drop/Components/Content.vue");
/* tslint:disable-next-line:no-var-requires */
const DraggableSource = require("../../../drag-and-drop/Components/Source.vue");

const Frame: ComponentOptions<any> = {
    name: "dm-window-frame",
    props: {
        uuid: {
            required: true,
            type: String,
            validator: (value: string): boolean => {
                return new RegExp(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/).test(value);
            },
        },
    },

    inject: [
        "draggableContext",
    ],

    components: {
        DraggableContent,
        DraggableSource,
    },
    computed: {
        coordinate() {
            return WindowStore.getters.getFrame(this.uuid).coordinate;
        },
        dimension() {
            return WindowStore.getters.getFrame(this.uuid).dimension;
        },
        content() {
            return WindowStore.getters.getContent(this.uuid);
        },
        maximized() {
            return WindowStore.getters.isMaximized(this.uuid);
        },
        style() {
            return {
                top: !this.maximized ? this.coordinate.y + "px" : null,
                left: !this.maximized ? this.coordinate.x + "px" : null,
                zIndex: this.coordinate.z,
                width: !this.maximized ? this.dimension.width + "px" : null,
                height: !this.maximized ? this.dimension.height + "px" : null,
            };
        },
    },
    methods: {
        drag(event: DragEvent) {
            let {x, y} = event;

            const contextWidth = this.draggableContext.dimensions.width;
            const contextHeight = this.draggableContext.dimensions.height;

            // Check for window constraints
            // TODO move constraint checking logic into store to prevent moving window out of desktop
            const halfWidth = Math.floor(this.dimension.width / 2);
            const halfHeight = Math.floor(this.dimension.height / 2);

            if (x < -halfWidth) {
                x = -halfWidth;
            } else if (x > contextWidth - halfWidth) {
                x = contextWidth - halfWidth;
            }

            if (y < 0) {
                y = 0;
            } else if (y > contextHeight - halfHeight) {
                y = contextHeight - halfHeight;
            }

            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("moveTo", {
                uuid: this.uuid,
                x,
                y,
            });
        },

        dragStart(event: MouseEvent) {
            this.$emit("draggable.drag", event);
        },

        resizeTop(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.TOP,
                uuid: this.uuid,
                y: event.y,
            });
        },

        resizeTopLeft(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.TOP_LEFT,
                uuid: this.uuid,
                x: event.x,
                y: event.y,
            });
        },

        resizeTopRight(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.TOP_RIGHT,
                uuid: this.uuid,
                x: event.x,
                y: event.y,
            });
        },

        resizeLeft(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.LEFT,
                uuid: this.uuid,
                x: event.x,
            });
        },

        resizeRight(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.RIGHT,
                uuid: this.uuid,
                x: event.x,
            });
        },

        resizeBottom(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.BOTTOM,
                uuid: this.uuid,
                y: event.y,
            });
        },

        resizeBottomLeft(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.BOTTOM_LEFT,
                uuid: this.uuid,
                x: event.x,
                y: event.y,
            });
        },

        resizeBottomRight(event: DragEvent) {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("resizeTo", {
                direction: ResizeDirection.BOTTOM_RIGHT,
                uuid: this.uuid,
                x: event.x,
                y: event.y,
            });
        },

        focus() {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("focus", {
                uuid: this.uuid,
            });
        },

        restoreMaximize() {
            if (WindowStore.getters.isMaximized(this.uuid)) {
                WindowStore.dispatch("restore", {uuid: this.uuid});
            } else {
                WindowStore.dispatch("maximize", {uuid: this.uuid});
            }
        },

        close() {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("close", {
                uuid: this.uuid,
            });
        },
    },
};

export default Frame;
