import {ComponentOptions} from "vue";

const Item: ComponentOptions<any> = {
    name: "dm-taskbar-item",
    props: {
        uuid: {
            required: true,
            type: String,
            validator: (value: string): boolean => {
                return new RegExp(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/).test(value);
            },
        },
    },
};

export default Item;
