import {ComponentOptions} from "vue";
import {WindowStore} from "../../Store/WindowStore";

/* tslint:disable-next-line:no-var-requires */
const DmTaskbarItem = require("./Item.vue");

const Taskbar: ComponentOptions<any> = {
    components: {
        DmTaskbarItem,
    },
    computed: {
        ids() {
            return WindowStore.state.ids;
        },
    },
};

export default Taskbar;
