import {ComponentOptions} from "vue";
import {WindowStore} from "../Store/WindowStore";

/* tslint:disable-next-line:no-var-requires */
const DmFrame = require("./Window/Frame.vue");
/* tslint:disable-next-line:no-var-requires */
const DmContext = require("../../drag-and-drop/Components/Context.vue");

const Desktop: ComponentOptions<any> = {
    name: "dm-desktop",
    components: {
        DmContext,
        DmFrame,
    },
    computed: {
        ids() {
            return WindowStore.state.ids;
        },
    },
};

export default Desktop;
