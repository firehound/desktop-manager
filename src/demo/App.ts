import {WindowStore} from "../dm/Store/WindowStore";

/* tslint:disable-next-line:no-var-requires */
const DmDesktop = require("../dm/Components/Desktop.vue");
/* tslint:disable-next-line:no-var-requires */
const DmTaskbar = require("../dm/Components/Taskbar/Taskbar.vue");

export default {
    name: "app",
    components: {
        DmDesktop,
        DmTaskbar,
    },
    methods: {
        open: () => {
            // noinspection JSIgnoredPromiseFromCall
            WindowStore.dispatch("open", {
                options: {
                    type: "sample",
                    frame: {
                        coordinate: {
                            x: 100,
                            y: 10,
                        },
                        dimension: {
                            width: 100,
                            height: 100,
                        },
                    },
                },
            });
        },
    },
};
