import {ConsoleHandler, logger} from "@vi-kon/logger";
import Vue from "vue";
import {WindowStore} from "../dm/Store/WindowStore";
import "../icomoon/style.less";

logger.setLogHandler(new ConsoleHandler());

/* tslint:disable-next-line:no-var-requires */
const App = require("./App.vue");

export const app = new Vue({
    el: "#root",
    render: (createElement: Vue.CreateElement) => {
        return createElement(App);
    },
});

WindowStore.dispatch("register", {
    type: "sample",
    content: require("./window/content/SampleContent.vue"),
});
