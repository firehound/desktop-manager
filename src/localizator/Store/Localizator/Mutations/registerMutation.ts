import Vue from "vue";
import {RegisterActionPayload} from "../Actions/registerAction";
import {HandlerMeta, HandlerStatus, LocalizatorState} from "../LocalizatorState";

export type RegisterMutationPayload = RegisterActionPayload;

export const registerMutation = (state: LocalizatorState, {locale, path, handler}: RegisterMutationPayload): void => {
    if (!state.handlers.hasOwnProperty(locale)) {
        Vue.set(state.handlers, locale, {});
    }

    const handlerMeta: HandlerMeta = {
        locale,
        path,
        status: HandlerStatus.REGISTERED,
        handler,
    };

    Vue.set(state.handlers[locale], path, handlerMeta);
};
