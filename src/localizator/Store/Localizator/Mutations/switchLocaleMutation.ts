import {SwitchActionPayload} from "../Actions/switchLocaleAction";
import {LocalizatorState} from "../LocalizatorState";

export type SwitchMutationPayload = SwitchActionPayload;

export const switchLocaleMutation = (state: LocalizatorState, {locale}: SwitchMutationPayload): void => {
    state.locale = locale;
};
