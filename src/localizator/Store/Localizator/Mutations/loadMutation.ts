import Vue from "vue";
import {LocalizatorState} from "../LocalizatorState";

export interface LoadMutationPayload {
    locale: string;
    localizations: { [key: string]: string };
}

export const loadMutation = (state: LocalizatorState, {locale, localizations}: LoadMutationPayload): void => {
    if (!state.localizations.hasOwnProperty(locale)) {
        Vue.set(state.localizations, locale, {});
    }

    Object.keys(localizations).map((key) => {
        if (state.localizations[locale].hasOwnProperty(key)) {
            state.localizations[locale][key] = localizations[key];
        } else {
            Vue.set(state.localizations[locale], key, localizations[key]);
        }
    });
};
