import {LocalizatorState} from "../LocalizatorState";

export type hasGetterSignature = (trans: string, locale?: string) => boolean;

export const hasGetter = (state: LocalizatorState): hasGetterSignature => {
    return (trans: string, locale?: string): boolean => {
        locale = locale || state.locale;

        return state.localizations.hasOwnProperty(locale) && state.localizations[locale].hasOwnProperty(trans);
    };
};
