import {LocalizatorGetters} from "../../Localizator";
import {loadFor} from "../Helper/loadFor";
import {replaceAttributesInLocalization} from "../Helper/replaceAttributesInLocalization";
import {LocalizatorState} from "../LocalizatorState";

export interface TransAttributes {
    [key: string]: string | number;
}

export type transGetterSignature = (key: string, attributes?: TransAttributes, locale?: string) => string;

export const transGetter = (state: LocalizatorState, getters: LocalizatorGetters): transGetterSignature => {
    return (key: string, attributes?: TransAttributes, locale?: string): string => {
        locale = locale || state.locale;
        if (getters.has(key, locale)) {
            return replaceAttributesInLocalization(state.localizations[locale][key], attributes);
        }
        loadFor(state, locale, key);

        locale = state.fallbackLocale;
        if (getters.has(key, locale)) {
            return replaceAttributesInLocalization(state.localizations[locale][key], attributes);
        }
        loadFor(state, locale, key);

        return key;
    };
};
