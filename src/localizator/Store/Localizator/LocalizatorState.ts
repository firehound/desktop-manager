import {ActionContext} from "vuex";

export enum HandlerStatus {
    REGISTERED,
    LOADING,
    LOADED,
}

export type HandlerSignature = () => Promise<{ [key: string]: string }>;

export interface HandlerMeta {
    locale: string;
    path: string;
    status: HandlerStatus;
    handler: HandlerSignature;
}

export interface LocalizatorState {
    locale: string;
    fallbackLocale: string;
    handlers: {
        [key: string]: {
            [key: string]: HandlerMeta;
        };
    };
    localizations: {
        [key: string]: {
            [key: string]: string;
        };
    };
}

export type LocalizatorActionContext = ActionContext<LocalizatorState, LocalizatorState>;
