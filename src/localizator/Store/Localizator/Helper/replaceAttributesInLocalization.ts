import {TransAttributes} from "../Getters/transGetter";

export const replaceAttributesInLocalization = (localization: string, attrs?: TransAttributes): string => {
    if (attrs) {
        Object.keys(attrs).map((key) => {
            localization = localization.replace(":" + key, attrs[key].toString());
        });
    }

    return localization;
};
