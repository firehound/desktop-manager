import {HandlerMeta, LocalizatorState} from "../LocalizatorState";
import escape = require("escape-string-regexp");

export const findBestHandlerFor = (state: LocalizatorState, locale: string, key: string): HandlerMeta | null => {
    if (state.handlers.hasOwnProperty(locale)) {
        let longestPath = "";

        Object.keys(state.handlers[locale]).forEach((path) => {
            if (path.length > longestPath.length && (new RegExp("^" + escape(path))).test(key)) {
                longestPath = path;
            }
        });

        if (longestPath) {
            return state.handlers[locale][longestPath];
        }
    }

    return null;
};
