import {Localizator} from "../../Localizator";
import {HandlerStatus, LocalizatorState} from "../LocalizatorState";
import {findBestHandlerFor} from "./findBestHandlerFor";

export const loadFor = (state: LocalizatorState, locale: string, name: string): void => {
    // We assumed that keyed localization is found in the best matching handler
    const bestHandler = findBestHandlerFor(state, locale, name);

    if (bestHandler && bestHandler.status === HandlerStatus.REGISTERED) {
        // noinspection JSIgnoredPromiseFromCall
        Localizator.dispatch("load", {
            locale: bestHandler.locale,
            path: bestHandler.path,
        });
    }
};
