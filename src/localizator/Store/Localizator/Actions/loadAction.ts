import {logger} from "@vi-kon/logger";
import {HandlerStatus, LocalizatorActionContext} from "../LocalizatorState";
import flatten = require("flat");

export interface LoadActionPayload {
    locale: string;
    path: string;
}

export const loadAction = ({commit, state}: LocalizatorActionContext, {locale, path}: LoadActionPayload): void => {
    if (!state.handlers.hasOwnProperty(locale) || !state.handlers[locale].hasOwnProperty(path)) {
        return;
    }

    const handler = state.handlers[locale][path];

    switch (handler.status) {
        case HandlerStatus.REGISTERED:
            logger.debug("Loading \"" + locale + ":" + path + "\" localization", {tags: ["localization"]});

            handler.status = HandlerStatus.LOADING;

            handler.handler()
                .then((json) => {
                    const localizations = flatten({[path]: json});

                    handler.status = HandlerStatus.LOADED;

                    commit("load", {locale, localizations});
                })
                .catch((error: Error) => {
                    logger.error("Error during loading \"" + locale + ":" + path + "\" localization", {
                        tags: ["localization"],
                        error,
                    });
                });
            break;

        case HandlerStatus.LOADED:
            logger.warn("Try to load already loaded \"" + locale + ":" + path + "\" localization", {
                tags: ["localization"],
            });
            break;

    }
};
