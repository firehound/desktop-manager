import {HandlerSignature, LocalizatorActionContext} from "../LocalizatorState";

export interface RegisterActionPayload {
    locale: string;
    path: string;
    handler: HandlerSignature;
}

export const registerAction = ({commit}: LocalizatorActionContext, payload: RegisterActionPayload): void => {
    commit("register", payload);
};
