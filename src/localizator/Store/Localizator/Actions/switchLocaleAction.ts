import {LocalizatorActionContext} from "../LocalizatorState";

export interface SwitchActionPayload {
    locale: string;
}

export const switchLocaleAction = ({commit}: LocalizatorActionContext, payload: SwitchActionPayload): void => {
    commit("switchLocale", payload);
};
