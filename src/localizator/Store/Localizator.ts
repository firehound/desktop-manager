import Vue from "vue";
import Vuex, {CommitOptions, DispatchOptions, Payload, Store} from "vuex";
import {loadAction, LoadActionPayload} from "./Localizator/Actions/loadAction";
import {registerAction, RegisterActionPayload} from "./Localizator/Actions/registerAction";
import {SwitchActionPayload, switchLocaleAction} from "./Localizator/Actions/switchLocaleAction";
import {hasGetter, hasGetterSignature} from "./Localizator/Getters/hasGetter";
import {transGetter, transGetterSignature} from "./Localizator/Getters/transGetter";
import {LocalizatorState} from "./Localizator/LocalizatorState";
import {loadMutation, LoadMutationPayload} from "./Localizator/Mutations/loadMutation";
import {registerMutation, RegisterMutationPayload} from "./Localizator/Mutations/registerMutation";
import {switchLocaleMutation, SwitchMutationPayload} from "./Localizator/Mutations/switchLocaleMutation";

/*
 * ---------------------------------------------------------------------
 * | TYPE / INTERFACE DEFINITIONS
 * ---------------------------------------------------------------------
 */

export interface LocalizatorGetters {
    has: hasGetterSignature;
    trans: transGetterSignature;
}

interface LocalizatorConstraint extends Store<LocalizatorState> {
    readonly getters: LocalizatorGetters;

    commit(type: "register", payload: RegisterMutationPayload, options?: CommitOptions): void;

    commit(type: "switch", payload: SwitchMutationPayload, options?: CommitOptions): void;

    commit(type: "load", payload: LoadMutationPayload, options?: CommitOptions): void;

    commit<P extends Payload>(payloadWithType: P, options?: CommitOptions): void;

    dispatch(type: "register", payload: RegisterActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "switch", payload: SwitchActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(type: "load", payload: LoadActionPayload, options?: DispatchOptions): Promise<any[]>;

    dispatch(payloadWithType: LoadActionPayload & { type: "load" }, options?: DispatchOptions): Promise<any[]>;

    dispatch<P extends Payload>(payloadWithType: P, options?: DispatchOptions): Promise<any[]>;
}

/*
 * ---------------------------------------------------------------------
 * | STORE DEFINITION
 * ---------------------------------------------------------------------
 */

Vue.use(Vuex);

export const Localizator: LocalizatorConstraint = new Store<LocalizatorState>({
    state: {
        locale: "en",
        fallbackLocale: "en",
        handlers: {},
        localizations: {},
    },
    getters: {
        has: hasGetter,
        trans: transGetter,
    },
    mutations: {
        register: registerMutation,
        switchLocale: switchLocaleMutation,
        load: loadMutation,
    },
    actions: {
        register: registerAction,
        switchLocale: switchLocaleAction,
        load: loadAction,
    },
});
