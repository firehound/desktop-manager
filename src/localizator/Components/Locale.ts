import {ComponentOptions, CreateElement} from "vue";
import {Localizator} from "../Store/Localizator";

const Locale: ComponentOptions<any> = {
    name: "locale",
    props: {
        transKey: {
            type: String,
            required: true,
        },
        attributes: {
            type: Object,
        },
        locale: {
            type: String,
        },
    },
    computed: {
        translation(): string {
            return Localizator.getters.trans(this.transKey, this.attributes, this.locale);
        },
    },
    render(createElement: CreateElement) {
        return createElement("span", null, this.translation);
    },
};

export default Locale;
