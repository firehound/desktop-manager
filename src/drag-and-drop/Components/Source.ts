import {ComponentOptions, CreateElement, VNode} from "vue";

export interface DragEvent {
    x: number;
    y: number;
    deltaX: number;
    deltaY: number;
    initMouseEvent: MouseEvent | null;
    mouseEvent: MouseEvent;
}

const Source: ComponentOptions<any> = {
    inject: [
        "draggableContent",
        "draggableContext",
    ],

    props: {
        start: {
            type: Function,
        },
        drag: {
            type: Function,
            required: true,
        },
        release: {
            type: Function,
        },
        withoutContent: {
            type: Boolean,
        },
        disabled: {
            type: Boolean,
        },
    },

    data() {
        return {
            initMouseEvent: null,
            dragging: false,
            offset: {
                x: 0,
                y: 0,
            },
        };
    },

    methods: {
        createMouseEvent(event: MouseEvent): DragEvent {
            if (!this.withoutContent && this.draggableContent && this.draggableContent.refresh) {
                this.draggableContent.refresh();
            }

            event.preventDefault();

            let x = event.clientX;
            let y = event.clientY;

            if (this.draggableContext && this.draggableContext.dimensions) {
                x -= this.draggableContext.dimensions.left;
                y -= this.draggableContext.dimensions.top;
            }

            x -= this.offset.x;
            y -= this.offset.y;

            return {
                x,
                y,
                deltaX: this.initMouseEvent ? event.clientX - this.initMouseEvent.clientX : 0,
                deltaY: this.initMouseEvent ? event.clientY - this.initMouseEvent.clientY : 0,
                initMouseEvent: this.initMouseEvent,
                mouseEvent: event,
            };
        },

        mouseDown(event: MouseEvent) {
            if (this.dragging) {
                return;
            }

            if (this.disabled) {
                return;
            }

            const dragEvent = this.createMouseEvent(event);

            this.initMouseEvent = event;
            this.dragging = true;

            if (!this.withoutContent && this.draggableContent && this.draggableContent.dimensions) {
                this.offset.x = event.clientX - this.draggableContent.dimensions.left;
                this.offset.y = event.clientY - this.draggableContent.dimensions.top;
            }

            if (this.start) {
                this.start(dragEvent);
            }
        },

        mouseMove(event: MouseEvent) {
            if (!this.dragging) {
                return;
            }

            const dragEvent = this.createMouseEvent(event);

            this.drag(dragEvent);
        },

        mouseUp(event: MouseEvent) {
            if (!this.dragging) {
                return;
            }

            const dragEvent = this.createMouseEvent(event);

            this.initMouseEvent = null;
            this.dragging = false;

            if (this.release) {
                this.release(dragEvent);
            }
        },
    },

    created() {
        window.addEventListener("mousemove", this.mouseMove);
        window.addEventListener("mouseup", this.mouseUp);
    },

    destroyed() {
        window.addEventListener("mousemove", this.mouseMove);
        window.removeEventListener("mouseup", this.mouseUp);
    },

    mounted() {
        this.$slots.default.forEach((slot: VNode) => {
            slot.elm.addEventListener("mousedown", this.mouseDown);
        });
    },

    render(createElement: CreateElement) {
        if (this.$slots.default && this.$slots.default.length === 1) {
            return this.$slots.default[0];
        }

        return createElement("div", null, this.$slots.default);
    },
};

export default Source;
