import {ComponentOptions, CreateElement} from "vue";
import getNodeDimensions = require("get-node-dimensions");

const Context: ComponentOptions<any> = {
    provide() {
        const draggableContext = {};

        Object.defineProperties(draggableContext, {
            dimensions: {
                enumerable: true,
                get: () => this.dimensions,
            },
        });

        return {draggableContext};
    },

    data() {
        return {
            dimensions: {
                width: 0,
                height: 0,
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
            },
        };
    },

    mounted() {
        const dimensions = getNodeDimensions(this.$el, {margin: true});

        this.dimensions.width = dimensions.width;
        this.dimensions.height = dimensions.height;
        this.dimensions.top = dimensions.top;
        this.dimensions.right = dimensions.right;
        this.dimensions.bottom = dimensions.bottom;
        this.dimensions.left = dimensions.left;
    },

    render(createElement: CreateElement) {
        // console.log(getNodeDimensions())

        if (this.$slots.default && this.$slots.default.length === 1) {
            return this.$slots.default[0];
        }

        return createElement("div", null, this.$slots.default);
    },
};

export default Context;
