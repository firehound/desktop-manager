import {ComponentOptions, CreateElement} from "vue";
import getNodeDimensions = require("get-node-dimensions");

const Content: ComponentOptions<any> = {
    methods: {
        refresh() {
            const dimensions = getNodeDimensions(this.$el, {margin: true});

            this.dimensions.width = dimensions.width;
            this.dimensions.height = dimensions.height;
            this.dimensions.top = dimensions.top;
            this.dimensions.right = dimensions.right;
            this.dimensions.bottom = dimensions.bottom;
            this.dimensions.left = dimensions.left;
        },
    },

    provide() {
        const draggableContent = {};

        Object.defineProperties(draggableContent, {
            dimensions: {
                enumerable: true,
                get: () => this.dimensions,
            },
            refresh: {
                get: () => this.refresh,
            },
        });

        return {draggableContent};
    },

    data() {
        return {
            dimensions: {
                width: 0,
                height: 0,
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
            },
        };
    },

    mounted() {
        this.refresh();
    },

    render(createElement: CreateElement) {
        if (this.$slots.default && this.$slots.default.length === 1) {
            return this.$slots.default[0];
        }

        return createElement("div", null, this.$slots.default);
    },
};

export default Content;
